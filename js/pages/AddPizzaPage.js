import Page from './Page.js';
import PageRenderer from '../PageRenderer.js';

export default class AddPizzaPage extends Page {
	onSubmit;

	constructor() {
		super('Ajouter une pizza');
		this.submit = this.submit.bind(this);
	}

	render() {
		return /*html*/ `
        <form class="addPizzaPage">
        <label>
            Nom :
            <input type="text" name="nom" class="form-control">
        </label>
        <label>
            Base :
            <select name="base" class="form-control">
                <option>Tomate</option>
                <option>Crème</option>
            </select>
        </label>
        <label>
            Image :
            <input type="text" name="image" class="form-control" placeholder="https://...">
        </label>
        <label>
            Prix petit format :
            <input type="number" name="prix_petite" class="form-control" step="0.05">
        </label>
        <label>
            Prix grand format :
            <input type="number" name="prix_grande" class="form-control" step="0.05">
        </label>
        <label>
            Ingrédients :
            <select name="ingredients" multiple="true" class="form-control">
                <option value="1">Mozzarella</option>
                <option value="2">Jambon</option>
                <option value="3">Champignon</option>
                <option value="4">Olives</option>
            </select>
        </label>
        <button type="submit" class="btn btn-default">Ajouter</button>
        <a class="btn btn-default btn-ingredient">Ajouter Ingrédients</a>
    </form>
    `;
	}

	renderIngredient() {
		return /*html*/ `
        <form class="addIngredientPage">
        <label>
            Nom :
            <input type="text" name="nom" class="form-control">
        </label>
        <button type="submit" class="btn btn-default">Ajouter</button>
    </form>
    `;
	}

	mount(element) {
		const form = document.querySelector('.addPizzaPage');
		form.addEventListener('submit', this.submit);
		const pageIngredient = this.renderIngredient();
		document.querySelector('.btn-ingredient');
	}

	submit(event) {
		event.preventDefault();
		const nom = document.querySelector('input[name="nom"]').value;
		const base = document.querySelector('select[name="base"]').selectedOptions;
		const image = document.querySelector('input[name="image"]').value;
		const prix_petite = document.querySelector('input[name="prix_petite"]')
			.value;
		const prix_grande = document.querySelector('input[name="prix_grande"]')
			.value;
		const ingredients = document.querySelector('select[name="ingredients"]')
			.selectedOptions;

		if (
			nom == '' ||
			base.length == 0 ||
			image == '' ||
			prix_petite == '' ||
			prix_grande == '' ||
			ingredients.length == 0
		) {
			alert('Il manque des valeurs Timothé');
		} else {
			console.log(
				"Demande d'ajout de :" +
					nom +
					base +
					image +
					prix_petite +
					prix_grande +
					ingredients
			);
			event.currentTarget.reset();
			debugger;
			this.onSubmit(nom, base, image, prix_petite, prix_grande, ingredients);
		}
	}
}

import data from './data.js';
import HomePage from './pages/HomePage.js';
import PageRenderer from './PageRenderer.js';
import AddPizzaPage from './pages/AddPizzaPage.js';

// Initialisation du PageRenderer
PageRenderer.titleElement = document.querySelector('.pageTitle');
PageRenderer.contentElement = document.querySelector('.pizzasContainer');

// Affichage de la HomePage
const homePage = new HomePage(data);
const pizzaPage = new AddPizzaPage();
PageRenderer.renderPage(homePage); // affiche la liste des pizzas

pizzaPage.onSubmit = function(
	nom,
	base,
	image,
	prix_petite,
	prix_grande,
	ingredients
) {
	const pizza = {
		nom,
		base,
		prix_petite: Number(prix_petite),
		prix_grande: Number(prix_grande),
		image,
	};
	data.push(pizza);
	homePage.pizzas = data;
	PageRenderer.renderPage(homePage);
};

//
// On aurait aussi pu faire ceci :
//
// const homePage = new HomePage([]);
// homePage.pizzas = data;
// PageRenderer.renderPage(homePage); // affiche la liste des pizzas

// --- Partie A --- //
//console.log(document.querySelector('.pageTitle'));
//console.log(document.querySelector('img'));
//console.log(document.querySelector('.pizzaFormButton'));
//console.log(document.querySelector('footer a'));
//console.log(document.querySelector('h4'));
//console.log(document.querySelectorAll('li a'));
//console.log(document.querySelectorAll('section ul li'));

document.querySelector(
	'.navbar-brand'
).innerHTML += `<small class="label label-success">les pizzas c'est la vie</small>`;
document.querySelector('h4').innerHTML = `<h4>Savoyarde</h4>`;
document.querySelector('li').setAttribute('class', 'active');

console.log(document.querySelectorAll('footer a')[1]);

function handleClick(event) {
	event.preventDefault();
	console.log(event);
}
const link = document.querySelector('a');
link.addEventListener('click', handleClick);

function messageClick(event) {
	event.preventDefault();
	console.log(event.currentTarget.innerHTML);
	document.querySelectorAll('.navbar-right li').forEach(element => {
		element.setAttribute('class', '');
	});
	event.currentTarget.setAttribute('class', 'active');
}

document.querySelectorAll('.navbar-right li').forEach(element => {
	element.addEventListener('click', messageClick);
});

document
	.querySelector('.newsContainer')
	.setAttribute('style', 'display:initial');

function closeNewsContainer(event) {
	event.preventDefault();
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
}

function showAddPizza(event) {
	PageRenderer.renderPage(pizzaPage);
}

document
	.querySelector('.closeButton')
	.addEventListener('click', closeNewsContainer);

document
	.querySelector('.pizzaFormButton')
	.addEventListener('click', showAddPizza);
